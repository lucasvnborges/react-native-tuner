&nbsp;

# Stringfy <img src="./assets/icon-1.png" width="110" align="left">

[![styled with PAPER](https://img.shields.io/badge/styled_with-Paper-6750a4.svg)](https://reactnativepaper.com/)

&nbsp;

Stringfy é um aplicativo mobile React-Native para músicos afinarem seus instrumentos de cordas de forma rápida e conveniente, oferecendo mais de 50 afinações para vários instrumentos e uma ferramenta de metrônomo para treinamento de andamento musical.
&nbsp;

<p align="center" margin-bottom="0">
  <a href="https://play.google.com/store/apps/details?id=com.hapbit.afinador">
    <img alt="Stringfy" width="820" height="auto" src="./assets/cover.png">
  </a>
</p>

&nbsp;
### Baixe o app grátis

<p align="center" margin-bottom="0" style="text-align: center;">
  <a href="https://play.google.com/store/apps/details?id=com.hapbit.afinador">
    <img alt="Stringfy" width="auto" height="40" src="./assets/google-play-badge.png">
  </a>
</p>

&nbsp;
### Tecnologias / Stack

- React-Native e Java
- React Hooks
- Todas as animações `Animated` estão usando o driver nativo
- Zustand `Context` + MMKV `Storage`
- i18next para suportar os idiomas inglês, português e espanhol
- adapty.io para suportar `inAppPurchases`

&nbsp;
### Contribuir

Baixe o app na Play Store que me ajuda a evoluir bastante. Feedbacks sempre são bem-vindos! ;)
